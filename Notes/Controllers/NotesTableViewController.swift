//
//  NotesTableViewController.swift
//  Notes
//
//  Created by Filip Klembara on 06/03/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import UIKit

class NotesTableViewController: UITableViewController {

    var model: NoteModel!

    private struct Segues {
        private init() { }
        static let newNoteSegue = "NewNoteSegue"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        clearsSelectionOnViewWillAppear = false
        navigationItem.rightBarButtonItems?.append(editButtonItem)
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Segues.newNoteSegue,
            let vc = segue.destination as? NewNoteViewController else {

                return
        }
        vc.delegate = self
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath)
        let note = model[indexPath.row]
        cell.detailTextLabel?.text = note.text
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        
        cell.textLabel?.text = dateFormatter.string(from: note.date)
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    public override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    // Override to support editing the table view.
    public override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            model.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // nope
        }
    }
}
