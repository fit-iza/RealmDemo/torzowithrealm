//
//  GroupsTableViewController.swift
//  Notes
//
//  Created by Filip Klembara on 10/04/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import UIKit

class GroupsTableViewController: UITableViewController {

    static let noteDetailSegue = "NoteDetail"

    let model = GroupModel.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        clearsSelectionOnViewWillAppear = false
        navigationItem.rightBarButtonItems?.append(editButtonItem)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == GroupsTableViewController.noteDetailSegue,
            let vc = segue.destination as? NotesTableViewController,
            let cell = sender as? UITableViewCell,
            let indexPath = tableView.indexPath(for: cell) else {

                return
        }
        vc.model = NoteModel(group: model[indexPath.row])
    }
    
    @IBAction func newGroup(_ sender: UIBarButtonItem) {
        let av = UIAlertController(title: "New group", message: nil, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let ok = UIAlertAction(title: "Create", style: .default) { _ in
            guard let name = av.textFields?.first?.text, name != "" else {
                return
            }
            self.model.insert(note: Group(name: name))
            let indexPath = IndexPath(row: 0, section: 0)
            self.tableView.insertRows(at: [indexPath], with: .fade)
        }
        av.addTextField { (field) in
            field.placeholder = "Your great new group name!"
        }
        av.addAction(cancel)
        av.addAction(ok)
        present(av, animated: true, completion: nil)
    }
    
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath)
        let group = model[indexPath.row]
        cell.detailTextLabel?.text = group.notes.count.description

        cell.textLabel?.text = group.name
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    public override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    // Override to support editing the table view.
    public override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            model.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // nope
        }
    }
}
