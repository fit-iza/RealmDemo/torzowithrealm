//
//  GroupModel.swift
//  Notes
//
//  Created by Filip Klembara on 10/04/2019.
//  Copyright © 2019 Filip Klembara. All rights reserved.
//

import Foundation
import UIKit

public class GroupModel: NSObject {
    private override init() { }

    public static let shared = GroupModel() // singleton

    private var groups = [
        Group(name: "Shop", notes: [
            Note(date: Date(), text: "Buy some milk"),
            Note(date: Date() - 3600 * 24 * 3, text: "Buy a Dogo")
            ])
    ]

    public var count: Int {
        return groups.count
    }

    public subscript(index: Int) -> Group {
        return groups[index]
    }
    
    public func insert(note: Group) {
        groups.insert(note, at: groups.startIndex)
    }
    
    public func remove(at index: Int) {
        groups.remove(at: index)
    }
}
